-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.14-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for notekeeping
CREATE DATABASE IF NOT EXISTS `notekeeping` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `notekeeping`;

-- Dumping structure for table notekeeping.access_master
CREATE TABLE IF NOT EXISTS `access_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table notekeeping.assigned_access_master
CREATE TABLE IF NOT EXISTS `assigned_access_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note_id` int(11) NOT NULL DEFAULT '0',
  `assigned_user_id` int(11) NOT NULL DEFAULT '0',
  `access_type_id` int(11) NOT NULL DEFAULT '0',
  `creation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_assigned_access_master_notes_master` (`note_id`),
  KEY `FK_assigned_access_master_users` (`assigned_user_id`),
  KEY `FK_assigned_access_master_access_master` (`access_type_id`),
  KEY `FK_assigned_access_master_users_2` (`created_by`),
  CONSTRAINT `FK_assigned_access_master_access_master` FOREIGN KEY (`access_type_id`) REFERENCES `access_master` (`id`),
  CONSTRAINT `FK_assigned_access_master_notes_master` FOREIGN KEY (`note_id`) REFERENCES `notes_master` (`id`),
  CONSTRAINT `FK_assigned_access_master_users` FOREIGN KEY (`assigned_user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FK_assigned_access_master_users_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table notekeeping.notes_master
CREATE TABLE IF NOT EXISTS `notes_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note_subject` varchar(50) DEFAULT NULL,
  `note` varchar(10000) DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `updation_time` datetime DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_notes_master_users` (`user_id`),
  KEY `FK_notes_master_tags_master` (`tag_id`),
  CONSTRAINT `FK_notes_master_tags_master` FOREIGN KEY (`tag_id`) REFERENCES `tags_master` (`id`),
  CONSTRAINT `FK_notes_master_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table notekeeping.tags_master
CREATE TABLE IF NOT EXISTS `tags_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(50) DEFAULT NULL,
  `creation_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updation_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users` (`created_by`),
  CONSTRAINT `FK__users` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table notekeeping.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(250) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL,
  `email_id` varchar(50) NOT NULL,
  `phone_no` varchar(15) DEFAULT NULL,
  `creation_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updation_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`email_id`),
  UNIQUE KEY `user-unique` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
