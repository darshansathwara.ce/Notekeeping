package com.kiprosh.notekeeping.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Tags;
import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.dto.TagsDTO;
import com.kiprosh.notekeeping.service.NotesService;
import com.kiprosh.notekeeping.service.TagsService;
import com.kiprosh.notekeeping.utils.UserTokenParser;

/**
 * @author darshan.sathwara
 *
 */
@RestController
public class TagController {

	@Autowired
	private TagsService tagsService;

	@Autowired
	private NotesService notesService;

	@RequestMapping(value = "/add-tag", method = RequestMethod.POST, headers = "Accept=application/json", produces = {
			"application/json" })
	public ResponseEntity<Users> addNote(@RequestBody Tags tags,
			HttpServletRequest request) {
		ResponseEntity<Users> response;

		Integer userId = UserTokenParser.getUserIdFromToken(request);
		Users user = new Users();
		user.setUserId(userId);

		tags.setUserId(user);
		tags.setCreationTime(new Date());
		tags.setUpdationTime(new Date());
		tagsService.saveNote(tags);

		response = new ResponseEntity<>(null, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/get-all-tags", method = RequestMethod.POST)
	public ResponseEntity<List<TagsDTO>> getAllNote(HttpServletRequest request) {

		Integer userId = UserTokenParser.getUserIdFromToken(request);

		Users user = new Users();
		user.setUserId(userId);

		List<Tags> tagsList = tagsService.getAllTags(user);
		ResponseEntity<List<TagsDTO>> response;

		int count = 0;
		List<TagsDTO> dtos = new ArrayList<>();
		TagsDTO dto;

		for (Tags tags : tagsList) {
			count = notesService.getTagCountForNote(tags);
			dto = new TagsDTO();
			dto.setId(tags.getId());
			dto.setTagName(tags.getTagName());
			dto.setUserId(tags.getUserId());
			dto.setNoteCount(count);
			dto.setUpdationTime(tags.getUpdationTime());
			dtos.add(dto);
		}

		response = new ResponseEntity<>(dtos, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/delete-tag", method = RequestMethod.POST, headers = "Accept=application/json", produces = {
			"application/json" })
	public ResponseEntity<List<Notes>> deleteNote(@RequestBody Tags tag,
			HttpServletRequest request) {

		tagsService.deleteTag(tag);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
