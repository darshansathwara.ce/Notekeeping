package com.kiprosh.notekeeping.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kiprosh.notekeeping.domain.AccessType;
import com.kiprosh.notekeeping.domain.AssignedAccess;
import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Tags;
import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.service.AssignAccessService;
import com.kiprosh.notekeeping.service.NotesService;
import com.kiprosh.notekeeping.utils.UserTokenParser;

/**
 * @author darshan.sathwara
 *
 */
@RestController
public class NoteController {

	@Autowired
	private NotesService notesService;

	@Autowired
	private AssignAccessService accessService;

		@RequestMapping(value = "/add-note", method = RequestMethod.POST, consumes = {
				"multipart/mixed", "multipart/form-data" })
		public ResponseEntity<Users> addNote(@RequestParam("noteSubject") String noteSubject,
				@RequestParam("note") String note,
				@RequestParam("tagId") int tagId, @RequestParam("noteId") int noteId ,  HttpServletRequest request) {
		ResponseEntity<Users> response;
		Boolean updateAccessTye = false;

		Integer userId = UserTokenParser.getUserIdFromToken(request);
		Users users = new Users();
		users.setUserId(userId);
		
		Notes notes = new Notes();
		notes.setNoteSubject(noteSubject);
		notes.setNote(note);
		
		Tags tag = new Tags();
		tag.setId(tagId);
		
		System.out.println(noteId + "-----------------------");
		
		notes.setTagId(tag);

		if (notes.getId() == 0) {
			updateAccessTye = true;
		}
		
		notes.setUsers(users);
		notes.setCreationTime(new Date());
		notes.setUpdationTime(new Date());
		Notes notes2 = notesService.saveNote(notes);

		if (updateAccessTye) {
			AssignedAccess assignedAccess = new AssignedAccess();
			assignedAccess.setNoteId(notes2);
			assignedAccess.setAssignedUserId(users);

			AccessType accessType = new AccessType();
			accessType.setId(3);
			assignedAccess.setAccessTypeId(accessType);
			assignedAccess.setCreationTime(new Date());
			assignedAccess.setUpdationTime(new Date());
			assignedAccess.setUsers(users);
			accessService.assignNote(assignedAccess);

		}

		response = new ResponseEntity<>(null, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/get-all-notes", method = RequestMethod.POST)
	public ResponseEntity<List<AssignedAccess>> getAllNote(HttpServletRequest request) {
		Integer userId = UserTokenParser.getUserIdFromToken(request);

		List<AssignedAccess> notesList = notesService.getAllNotes(userId);
		ResponseEntity<List<AssignedAccess>> response;

		response = new ResponseEntity<>(notesList, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/get-all-notes-for-assign", method = RequestMethod.POST)
	public ResponseEntity<List<AssignedAccess>> getAllNoteForAssign(
			HttpServletRequest request) {
		Integer userId = UserTokenParser.getUserIdFromToken(request);

		List<AssignedAccess> notesList = notesService.getAllNotesForAssign(userId, 3);
		ResponseEntity<List<AssignedAccess>> response;

		response = new ResponseEntity<>(notesList, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/delete-note", method = RequestMethod.POST, headers = "Accept=application/json", produces = {
			"application/json" })
	public ResponseEntity<List<Notes>> deleteNote(@RequestBody Notes notes,
			HttpServletRequest request) {

		List<AssignedAccess> accessList = accessService.findByNoteId(notes);
		
		accessList.forEach(access -> accessService.delete(access));
		
		notesService.deleteNote(notes);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
}
