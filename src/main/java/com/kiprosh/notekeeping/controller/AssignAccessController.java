package com.kiprosh.notekeeping.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kiprosh.notekeeping.domain.AccessType;
import com.kiprosh.notekeeping.domain.AssignedAccess;
import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.service.AssignAccessService;
import com.kiprosh.notekeeping.utils.UserTokenParser;
/**
 * @author darshan.sathwara
 *
 */
@RestController
public class AssignAccessController {

	@Autowired
	private AssignAccessService assignAccessService;

	@RequestMapping(value = "/assign-note", method = RequestMethod.POST, consumes = {
			"multipart/mixed", "multipart/form-data" })
	public ResponseEntity<Users> addNote(@RequestParam("noteId") int noteId,
			@RequestParam("assignedUserId") int assignedUserId,
			@RequestParam("accessTypeId") int accessTypeId, HttpServletRequest request) {
		ResponseEntity<Users> response;
		Integer userId = UserTokenParser.getUserIdFromToken(request);
		Users users = new Users();
		users.setUserId(userId);

		AssignedAccess assignedAccess = new AssignedAccess();

		Notes note = new Notes();
		note.setId(noteId);
		assignedAccess.setNoteId(note);

		Users users2 = new Users();
		users2.setUserId(assignedUserId);
		assignedAccess.setAssignedUserId(users2);

		AccessType accessType = new AccessType();
		accessType.setId(accessTypeId);

		assignedAccess.setAccessTypeId(accessType);
		assignedAccess.setUsers(users);
		assignedAccess.setCreationTime(new Date());
		assignedAccess.setUpdationTime(new Date());
		
		AssignedAccess alreadyAvailable = assignAccessService.findByNoteIdAndAssignedUserId(note, users2);
		if(alreadyAvailable == null) {
			assignAccessService.assignNote(assignedAccess);
			response = new ResponseEntity<>(null, HttpStatus.OK);
		}else {
			response = new ResponseEntity<>(null, HttpStatus.CONFLICT);
		}		return response;
	}

	@RequestMapping(value = "/get-all-assigned-notes", method = RequestMethod.GET)
	public ResponseEntity<List<AssignedAccess>> getAllNote(HttpServletRequest request) {

		Integer userId = UserTokenParser.getUserIdFromToken(request);
		Users users = new Users();
		users.setUserId(userId);
		
		List<AssignedAccess> notesList = assignAccessService.getAllAssignedNotes(users);
		ResponseEntity<List<AssignedAccess>> response;

		response = new ResponseEntity<>(notesList, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/delete-assigned-note", method = RequestMethod.POST, headers = "Accept=application/json", produces = {
			"application/json" })
	public ResponseEntity<List<Notes>> deleteNote(
			@RequestBody AssignedAccess assignedAccess, HttpServletRequest request) {

		assignAccessService.delete(assignedAccess);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
