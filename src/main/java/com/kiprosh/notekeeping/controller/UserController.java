package com.kiprosh.notekeeping.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.service.UserService;
import com.kiprosh.notekeeping.utils.PasswordUtility;
import com.kiprosh.notekeeping.utils.UserTokenParser;

/**
 * @author darshan.sathwara
 *
 */
@RestController
public class UserController {

	private static final Logger LOGGER = Logger.getLogger(UserController.class);

	/**
	 * 
	 */
	@Autowired
	private UserService userService;

	/**
	 * @param request
	 * @return
	 * 
	 * This method used to get all users from database
	 */

	/**
	 * @param user This method used to add users to database
	 */
	@RequestMapping(value = "/add-user", method = RequestMethod.POST)
	public ResponseEntity<Users> addUser(@RequestBody Users user,
			HttpServletRequest request) {
		LOGGER.info("Add user " + request);
		ResponseEntity<Users> response;
		user.setPassword(PasswordUtility.passwordEncoder(user.getPassword()));
		user.setUpdationTime(new Date());
		user.setCreationTime(new Date());

		Users u = userService.findUserByEmailId(user.getEmail());
		if(u ==  null) {
			response = new ResponseEntity<>(userService.save(user), HttpStatus.OK);	
		}else {
			response = new ResponseEntity<>(null, HttpStatus.CONFLICT);
		}
		
		return response;
	}

	/**
	 * @param user
	 * @return This method used to change the password as requested by user.
	 */
	@RequestMapping(value = "/change-password", method = RequestMethod.POST)
	public ResponseEntity<UserController> changePassword(
			@RequestParam("password") String password, HttpServletRequest request,
			@RequestParam("new_password") String newPassword) {
		LOGGER.info("In change password");
		LOGGER.info("----- IN change password -----");

		ResponseEntity<UserController> response = null;
		LOGGER.info("----- getting user id from token change password -----");
		Users users = userService.getUser(UserTokenParser.getUserIdFromToken(request));
		if (users != null) {

			if (PasswordUtility.matchPassword(password, users.getPassword())) {
				users.setPassword(PasswordUtility.passwordEncoder(newPassword));
				userService.save(users);
				LOGGER.info("----- password updated successfully -----");
				response = new ResponseEntity<>(HttpStatus.OK);

			}
			else {
				response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

		}
		return response;
	}

	@RequestMapping(value = "/get-all-users", method = RequestMethod.POST)
	public ResponseEntity<List<Users>> getAllNote(HttpServletRequest request) {

		List<Users> usersList = userService.getAllUsers();
		ResponseEntity<List<Users>> response;

		Integer userId = UserTokenParser.getUserIdFromToken(request);
		Users users;

		for (int i = 0; i < usersList.size(); i++) {
			users = usersList.get(i);
			if (users.getUserId() == userId) {
				usersList.remove(i);
			}
		}

		response = new ResponseEntity<>(usersList, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/get-loggedin-user", method = RequestMethod.GET)
	public ResponseEntity<Users> getCurrentLoggedInUser(HttpServletRequest request) {
		ResponseEntity<Users> response;
		LOGGER.info("----- IN get logged in user details  -----");
		Users users = userService.getUser(UserTokenParser.getUserIdFromToken(request));

		if (users != null) {

			users = userService.getUser(users.getUserId());
			LOGGER.info("-----IN GET logged in  user details -----");
			response = new ResponseEntity<>(users, HttpStatus.OK);
		}
		else {
			LOGGER.info("-----user not logged in-----");
			response = new ResponseEntity<>(users, HttpStatus.FORBIDDEN);
		}

		return response;
	}

}