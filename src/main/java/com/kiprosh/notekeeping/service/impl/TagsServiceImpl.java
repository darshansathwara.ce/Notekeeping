package com.kiprosh.notekeeping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.Tags;
import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.repositories.TagsRepository;
import com.kiprosh.notekeeping.service.TagsService;

/**
 * @author akash.shinde
 *
 */
@Service
public class TagsServiceImpl implements TagsService {

	@Autowired
	private TagsRepository tagsRepository;

	@Override
	public Tags saveNote(Tags tags) {
		return tagsRepository.save(tags);
	}

	@Override
	public List<Tags> getAllTags(Users user) {
		return (List<Tags>) tagsRepository.findByUserId(user);
	}

	@Override
	public void deleteTag(Tags tag) {
		tagsRepository.delete(tag);
	}

}
