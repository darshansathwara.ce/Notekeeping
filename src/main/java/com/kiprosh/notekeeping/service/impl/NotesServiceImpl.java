package com.kiprosh.notekeeping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.AccessType;
import com.kiprosh.notekeeping.domain.AssignedAccess;
import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Tags;
import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.repositories.AssignAccessRepository;
import com.kiprosh.notekeeping.repositories.NotesRepository;
import com.kiprosh.notekeeping.service.NotesService;

/**
 * @author akash.shinde
 *
 */
@Service
public class NotesServiceImpl implements NotesService {

	@Autowired
	private NotesRepository notesRepository;
	
	@Autowired
	private AssignAccessRepository accessRepository;

	@Override
	public Notes saveNote(Notes notes) {
		return notesRepository.save(notes);
	}

	@Override
	public List<AssignedAccess> getAllNotes(int userId) {
		Users user = new Users();
		user.setUserId(userId);
		return (List<AssignedAccess>) accessRepository.findByAssignedUserId(user);

	}

	@Override
	public void deleteNote(Notes notes) {
		notesRepository.delete(notes.getId());
	}

	@Override
	public List<AssignedAccess> getAllNotesForAssign(int userId, int accessTypeId) {
		Users user = new Users();
		user.setUserId(userId);
		
		AccessType accessType = new AccessType();
		accessType.setId(accessTypeId);
		return accessRepository.findByAssignedUserIdAndAccessTypeId(user, accessType);
	}

	@Override
	public int getTagCountForNote(Tags tag) {
		return notesRepository.getTagCountForNote(tag);
	}

}
