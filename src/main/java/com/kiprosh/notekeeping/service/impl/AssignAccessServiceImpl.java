package com.kiprosh.notekeeping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.AssignedAccess;
import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.repositories.AssignAccessRepository;
import com.kiprosh.notekeeping.service.AssignAccessService;

/**
 * @author akash.shinde
 *
 */
@Service
public class AssignAccessServiceImpl implements AssignAccessService {

	@Autowired
	private AssignAccessRepository accessRepository;

	@Override
	public List<AssignedAccess> getAllAssignedNotes(Users user) {
		return (List<AssignedAccess>) accessRepository.findByUsers(user);
	}

	@Override
	public void assignNote(AssignedAccess assignedAccess) {
		accessRepository.save(assignedAccess);

	}

	@Override
	public void delete(AssignedAccess assignedAccess) {
		accessRepository.delete(assignedAccess);
	}

	@Override
	public List<AssignedAccess> findByNoteId(Notes notes) {
		return accessRepository.findByNoteId(notes);
	}

	@Override
	public AssignedAccess findByNoteIdAndAssignedUserId(Notes notes, Users users) {
		return accessRepository.findByNoteIdAndAssignedUserId(notes, users);
	}

}
