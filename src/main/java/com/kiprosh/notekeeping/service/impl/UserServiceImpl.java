package com.kiprosh.notekeeping.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.Users;
import com.kiprosh.notekeeping.repositories.UserRepository;
import com.kiprosh.notekeeping.service.UserService;

/**
 * @author akash.shinde
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Users save(Users user) {
		return userRepository.save(user);
	}

	@Override
	public List<Users> getAllUsers() {
		return (List<Users>) userRepository.findAll();
	}

	@Override
	public Users getUser(long id) {
		return userRepository.findOne(id);
	}

	@Override
	public Users findUserByEmailId(String email) {
		return userRepository.findByEmail(email);
	}

}
