package com.kiprosh.notekeeping.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.Tags;
import com.kiprosh.notekeeping.domain.Users;

@Service
public interface TagsService {

	public Tags saveNote(Tags tags);
	
	public List<Tags> getAllTags(Users user);
	
	public void deleteTag(Tags tag);

}