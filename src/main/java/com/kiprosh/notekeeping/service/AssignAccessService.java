package com.kiprosh.notekeeping.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.AssignedAccess;
import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Users;

@Service
public interface AssignAccessService {

	public void assignNote(AssignedAccess assignedAccess);

	public List<AssignedAccess> getAllAssignedNotes(Users user);

	public void delete(AssignedAccess assignedAccess);

	public List<AssignedAccess> findByNoteId(Notes notes);
	
	public AssignedAccess findByNoteIdAndAssignedUserId(Notes notes,Users users);
}