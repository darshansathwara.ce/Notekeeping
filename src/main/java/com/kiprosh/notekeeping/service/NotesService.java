package com.kiprosh.notekeeping.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.AssignedAccess;
import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Tags;

@Service
public interface NotesService {

	public Notes saveNote(Notes notes);
	
	public List<AssignedAccess> getAllNotes(int userId);
	
	public List<AssignedAccess> getAllNotesForAssign(int userId , int accessTypeId);
	
	public void deleteNote(Notes notes);
	
	public int getTagCountForNote(Tags tag);
}