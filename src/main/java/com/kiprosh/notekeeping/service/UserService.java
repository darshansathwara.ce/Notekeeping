package com.kiprosh.notekeeping.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.kiprosh.notekeeping.domain.Users;

@Service
public interface UserService {

	/**
	 * Returns the list of users
	 * 
	 * @return
	 */
	public List<Users> getAllUsers();

	/**
	 * This method used save user
	 * 
	 * @param id
	 * @return
	 */
	public Users save(Users user);

	/**
	 * 
	 * This method used to get user details by id
	 * 
	 * @param user
	 * @return
	 */
	public Users getUser(long id);

	/**
	 * This method used to find user by email Id
	 * 
	 * @param email
	 * @return
	 */
	public Users findUserByEmailId(String email);

}