package com.kiprosh.notekeeping.utils;

import javax.servlet.http.HttpServletRequest;

import com.kiprosh.notekeeping.security.JWTParser;

import io.jsonwebtoken.Claims;

public final class UserTokenParser {

	// Default Constructor
	private UserTokenParser() {
		// Empty constructor
	}

	public static Integer getUserIdFromToken(HttpServletRequest request) {
		Claims body = JWTParser.tokenParser(request);
		Integer userId = (Integer) body.get("id");
		return userId;
	}
}
