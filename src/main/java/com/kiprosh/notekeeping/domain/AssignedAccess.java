package com.kiprosh.notekeeping.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 * @author darshan.sathwara
 *
 */
@Entity(name = "assigned_access_master")
public class AssignedAccess {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "note_id")
	private Notes noteId;

	@ManyToOne
	@JoinColumn(name = "assigned_user_id")
	private Users assignedUserId;

	@ManyToOne
	@JoinColumn(name = "access_type_id")
	private AccessType accessTypeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_time")
	private Date creationTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updation_time")
	private Date updationTime;

	@ManyToOne
	@JoinColumn(name = "created_by")
	private Users users;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Notes getNoteId() {
		return noteId;
	}

	public void setNoteId(Notes noteId) {
		this.noteId = noteId;
	}

	public Users getAssignedUserId() {
		return assignedUserId;
	}

	public void setAssignedUserId(Users assignedUserId) {
		this.assignedUserId = assignedUserId;
	}

	public AccessType getAccessTypeId() {
		return accessTypeId;
	}

	public void setAccessTypeId(AccessType accessTypeId) {
		this.accessTypeId = accessTypeId;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	@Override
	public String toString() {
		return "AssignedAccess [id=" + id + ", noteId=" + noteId + ", assignedUserId="
				+ assignedUserId + ", accessTypeId=" + accessTypeId + ", creationTime="
				+ creationTime + ", updationTime=" + updationTime + ", users=" + users
				+ "]";
	}

}
