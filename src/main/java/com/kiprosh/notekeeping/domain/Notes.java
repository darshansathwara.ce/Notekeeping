package com.kiprosh.notekeeping.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author darshan.sathwara
 *
 */
@Entity
@Table(name = "notes_master")
@NamedQuery(name = "Notes.findAll", query = "SELECT n FROM Notes n")
public class Notes {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "note_subject")
	private String noteSubject;

	@Column(name = "note")
	private String note;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_time")
	private Date creationTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updation_time")
	private Date updationTime;

	@ManyToOne
	@JoinColumn(name = "tag_id")
	private Tags tagId;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private Users users;

	public Notes() {

	}

	public String getNoteSubject() {
		return noteSubject;
	}

	public void setNoteSubject(String noteSubject) {
		this.noteSubject = noteSubject;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Tags getTagId() {
		return tagId;
	}

	public void setTagId(Tags tagId) {
		this.tagId = tagId;
	}

	@Override
	public String toString() {
		return "Notes [id=" + id + ", noteSubject=" + noteSubject + ", note=" + note
				+ ", creationTime=" + creationTime + ", updationTime=" + updationTime
				+ ", users=" + users + "]";
	}

}
