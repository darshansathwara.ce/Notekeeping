package com.kiprosh.notekeeping.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Tags;

@Repository
public interface NotesRepository extends CrudRepository<Notes, Integer> {

	@Query("select count(*) from Notes n where  n.tagId = ?1 ))")
	public int getTagCountForNote(Tags tag);
}