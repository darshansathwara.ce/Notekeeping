package com.kiprosh.notekeeping.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kiprosh.notekeeping.domain.AccessType;
import com.kiprosh.notekeeping.domain.AssignedAccess;
import com.kiprosh.notekeeping.domain.Notes;
import com.kiprosh.notekeeping.domain.Users;

@Repository
public interface AssignAccessRepository extends CrudRepository<AssignedAccess, Integer> {
	public List<AssignedAccess> findByAssignedUserId(Users userId);

	public List<AssignedAccess> findByUsers(Users users);

	public List<AssignedAccess> findByAssignedUserIdAndAccessTypeId(Users userId,
			AccessType accessType);

	public List<AssignedAccess> findByNoteId(Notes notes);
	
	public AssignedAccess findByNoteIdAndAssignedUserId(Notes notes , Users users);
}