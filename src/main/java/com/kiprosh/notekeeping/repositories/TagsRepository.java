package com.kiprosh.notekeeping.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kiprosh.notekeeping.domain.Tags;
import com.kiprosh.notekeeping.domain.Users;

@Repository
public interface TagsRepository extends CrudRepository<Tags, Integer> {
	
	public List<Tags> findByUserId(Users user);
}