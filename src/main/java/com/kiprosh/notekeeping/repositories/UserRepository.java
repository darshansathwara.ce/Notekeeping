package com.kiprosh.notekeeping.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kiprosh.notekeeping.domain.Users;

@Repository
public interface UserRepository extends CrudRepository<Users, Long> {
	public Users findByEmail(String username);

	public Users findByEmailAndPassword(String username, String passsword);

}