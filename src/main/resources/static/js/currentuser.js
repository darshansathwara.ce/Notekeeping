
$(document).ready(function() {

	// Increment the idle time counter every minute.
	var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

	// Zero the idle timer on mouse movement.
	$(this).mousemove(function(e) {
		idleTime = 0;
	});

	$(this).keypress(function(e) {
		idleTime = 0;
	});
});

$.ajax({
	type : 'GET',
	url : 'get-loggedin-user',
	contentType : 'application/json',
	headers : {
		"X-Auth-Token" : $.cookie("JSESSIONID")
	},
	success : function(data) {

		setTimeout(function() {
			$("#loggeduser").html("<b>Welcome : </b>" + data.firstName);
		}, 1000);
	},
	error : function(data) {
		document.cookie = 'JSESSIONID'
				+ '=; expires=Thu, 01-Jan-70 00:00:01 GMT;'
		window.location = "/kiprosh-notekeeping/login.html";
	}
});

if ($.cookie("JSESSIONID") == "") {
	window.location = "/kiprosh-notekeeping/login.html";
}

if (typeof $.cookie('JSESSIONID') === 'undefined') {
	window.location = "/kiprosh-notekeeping/login.html";
}

function timerIncrement() {
	idleTime = idleTime + 1;
	if (idleTime >= 60) { // 20 minutes
		$.cookie("JSESSIONID", "");
		document.cookie = 'JSESSIONID'
				+ '=; expires=Thu, 01-Jan-70 00:00:01 GMT;'
		logout();
	}
}

function logout() {
	$.ajax({
		type : 'GET',
		url : 'logout',
		contentType : 'application/json',
		success : function(data) {
			window.location = "/kiprosh-notekeeping/login.html";
		},
		error : function(data) {

		}
	});
	document.cookie = 'JSESSIONID' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;'
	window.location = "/kiprosh-notekeeping/login.html";
}
