//--for data table--//
$(document).ready(function() {
	$('#editAssignedNotes').hide();
});

loadNotestable();

function loadNotestable() {
	$
			.ajax({
				type : 'GET',
				url : 'get-all-assigned-notes',
				headers : {
					"X-Auth-Token" : $.cookie("JSESSIONID")
				},
				beforeSend : function() {
					$('#imgLoaderDiv').show();
				},
				complete : function() {
					$('#imgLoaderDiv').hide();
				},
				success : function(data) {
					console.log(data)
					$("#LoadingDiv").hide();
					$('#assignedNotesTable')
							.DataTable(
									{
										"scrollX" : true,
										"scrollY" : "300px",
										data : data,
										order : [ [ 1, "desc" ] ],
										columns : [
												{

													"data" : "id",
													render : function(data,
															type, row, meta) {
														return meta.row
																+ meta.settings._iDisplayStart
																+ 1;
													}
												},
												{
													data : "noteId.noteSubject",
													"render" : function(data,
															type, full, meta) {
														cropData = data.length > 10 ? data
																.substr(0, 10)
																+ '…'
																: data;
														return '<span data-toggle="tooltip" title="'
																+ data
																+ '">'
																+ cropData
																+ '</span>';
													}
												},
												{
													data : "assignedUserId",
													"render" : function(data,
															type, full, meta) {
														return data.firstName
																+ "  "
																+ data.lastName;
													}
												},
												{
													data : "accessTypeId.accessType",
													"render" : function(data,
															type, full, meta) {
														cropData = data.length > 10 ? data
																.substr(0, 10)
																+ '…'
																: data;
														return '<span data-toggle="tooltip" title="'
																+ data
																+ '">'
																+ cropData
																+ '</span>';
													}
												},
												{
													data : "updationTime",
													render : function(d) {
														return moment(d)
																.format(
																		"YYYY:MM:DD HH:mm");
													}
												},
												{
													"targets" : -1,
													"data" : null,
													"orderable" : false,
													"searchable" : false,
													"render" : function(data) {
														let action = "";
														action = "<a title='Delete' class='deleteBtn' style='cursor: pointer;'><i class='fa fa-remove'></i></a>";
														return action;
													}
												}

										]
									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					showAlert("Error:Server error occur", "alert-danger",
							"alertContainer", "alertRow");
				}

			// Edit record

			});
}

$('#assignedNotesTable').find('tbody').on(
		'click',
		'.deleteBtn',
		function() {
			if (!confirm("Are you sure?")) {
				return false;
			}
			const data = $('#assignedNotesTable').DataTable().row(
					$(this).parents('tr')).data();

			var id = data.id;

			const assignedNote = {
				"id" : id,
			};

			$.ajax({
				type : 'POST',
				url : 'delete-assigned-note',
				contentType : 'application/json',
				headers : {
					"X-Auth-Token" : $.cookie("JSESSIONID")
				},
				data : JSON.stringify(assignedNote),
				success : function(data, statusText, xhr) {
					showAlert("Successfully added note.", "alert-success",
							"alertContainer", "alertRow");

					const delay = 3000;
					setTimeout(function() {
						location.reload();
					}, delay);
				},
				error : function(data, statusText, xhr) {
					showAlert("An error occured while deleting note.",
							"alert-danger", "alertContainer", "alertRow");
				}
			});

		});
