$("#registerForm").submit(function(e) {
	e.preventDefault();
	registerUser();
});
function registerUser() {
	var authUser = {
		"firstName" : $("#firstName").val(),
		"lastName" : $("#lastName").val(),
		"phone" : $("#mobileNo").val(),
		"email" : $("#userLoginId").val(),
		"password" : $("#password").val()
	}

	$.ajax({
		type : 'POST',
		url : 'add-user',
		contentType : 'application/json',
		beforeSend : function() {
			$('#imgLoaderDiv').show();
		},
		complete : function() {
			$('#imgLoaderDiv').hide();
		},
		data : JSON.stringify(authUser),
		success : function(data, statusText, xhr) {
			showAlert("User added successfully", "alert-success",
					"alertContainer", "alertRow");
			const delay = 3000;
			setTimeout(function() {
				window.location.href = "login.html";

			}, delay);

		},
		error : function(data, statusText, xhr) {
			if(data.status == 409){
				showAlert("Error : Email-id is already registered. Please use another Email-id", "alert-danger",
						"alertContainer", "alertRow");
			}else{
				showAlert("Error : Error in registration", "alert-danger",
						"alertContainer", "alertRow");				
			}

		}
	});

	console.log(authUser);
}