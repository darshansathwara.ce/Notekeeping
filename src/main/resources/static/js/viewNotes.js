//--for data table--//
$(document).ready(function() {
	$('#editNote').hide();
	loadNotestable();
});



function loadNotestable() {
	$
			.ajax({
				type : 'POST',
				url : 'get-all-notes',
				headers : {
					"X-Auth-Token" : $.cookie("JSESSIONID")
				},
				beforeSend : function() {
					$('#imgLoaderDiv').show();
				},
				complete : function() {
					$('#imgLoaderDiv').hide();
				},
				success : function(data) {
					console.log(data)
					$("#LoadingDiv").hide();
					$('#notesTable')
							.DataTable(
									{
										"scrollX" : true,
										"scrollY" : "300px",
										data : data,
										order : [ [ 1, "desc" ] ],
										columns : [
												{
													"data" : "id",
													render : function(data,
															type, row, meta) {
														return meta.row
																+ meta.settings._iDisplayStart
																+ 1;
													}
												},
												{
													data : "noteId.noteSubject",
													"render" : function(data,
															type, full, meta) {
														cropData = data.length > 10 ? data
																.substr(0, 10)
																+ '…'
																: data;
														return '<span data-toggle="tooltip" title="'
																+ data
																+ '">'
																+ cropData
																+ '</span>';
													}
												},
												{
													data : "noteId.note",
													"render" : function(data,
															type, full, meta) {
														cropData = data.length > 10 ? data
																.substr(0, 10)
																+ '…'
																: data;
														return '<span data-toggle="tooltip" title="'
																+ data
																+ '">'
																+ cropData
																+ '</span>';
													}
												},
												{
													data : "noteId.tagId.tagName",
													render : function(data) {
														if (data != undefined) {
															return data
														} else {
															return ""
														}
													}

												},
												{
													data : "noteId.updationTime",
													render : function(d) {
														return moment(d)
																.format(
																		"YYYY:MM:DD HH:mm");
													}
												},
												{
													"targets" : -1,
													"data" : null,
													"orderable" : false,
													"searchable" : false,
													"render" : function(data) {
														let action = "";
														if (data.accessTypeId.id == 1) {
															action = "<a title='View' class='viewBtn' style='cursor: pointer;'><i class='fa fa-eye'></i></a>";
														} else {
															action = "<a title='View' class='viewBtn' style='cursor: pointer;'><i class='fa fa-eye'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a title='Edit' class='updateBtn' style='cursor: pointer;'><i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;&nbsp; <a title='Delete' class='deleteBtn' style='cursor: pointer;'><i class='fa fa-remove'></i></a>";
														}

														return action;
													}
												}

										]
									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					showAlert("Error:Server error occur", "alert-danger",
							"alertContainer", "alertRow");
				}

			// Edit record

			});
}
$('#notesTable').find('tbody').on(
		'click',
		'.deleteBtn',
		function() {
			if (!confirm("Are you sure?")) {
				return false;
			}
			const data = $('#notesTable').DataTable()
					.row($(this).parents('tr')).data();

			var id = data.noteId.id;

			const notes = {
				"id" : id,
			};

			$.ajax({
				type : 'POST',
				url : 'delete-note',
				contentType : 'application/json',
				headers : {
					"X-Auth-Token" : $.cookie("JSESSIONID")
				},
				data : JSON.stringify(notes),
				success : function(data, statusText, xhr) {
					showAlert("Successfully delete note.", "alert-success",
							"alertContainer", "alertRow");

					const delay = 3000;
					setTimeout(function() {
						location.reload();
					}, delay);
				},
				error : function(data, statusText, xhr) {
					showAlert("An error occured while deleting note.",
							"alert-danger", "alertContainer", "alertRow");
				}
			});

		});

$('#notesTable').find('tbody').on(
		'click',
		'.updateBtn',
		function() {

			const data = $('#notesTable').DataTable()
					.row($(this).parents('tr')).data();

			$("#noteSubject").val(data.noteId.noteSubject);
			$("#note").val(data.noteId.note);
			$("#noteid").val(data.noteId.id);
			if(data.noteId.tagId != undefined){
				$("#tagId").val(data.noteId.tagId.id);	
			}
			

			$('#editNote').show();

		});

$('#notesTable').find('tbody').on(
		'click',
		'.viewBtn',
		function() {
			const data = $('#notesTable').DataTable()
					.row($(this).parents('tr')).data();
			$("#viewNote").modal('show');

			$("#noteSubjectView").val(data.noteId.noteSubject);
			$("#noteView").val(data.noteId.note);
			$("#noteid").val(data.noteId.id);
			if(data.noteId.tagId != undefined){
				$("#tagIds").val(data.noteId.tagId.tagName);	
			}
			

		});

$('#btnReset').click(function() {
	$("#noteSubject").val('');
	$("#note").val('');
	$("#noteid").val('');

	$('#editNote').hide();
});

$('#btnSave').click(
		function() {
			var noteSubject = $("#noteSubject").val();
			var note = $("#note").val();
			var noteid = $("#noteid").val();
			var tagId = $("#tagId").val();

			let form = new FormData();

			form.append("noteSubject", noteSubject);
			form.append("note", note);
			form.append("tagId", tagId);
			form.append("noteId", noteid);

			$.ajax({
				type : 'POST',
				url : 'add-note',
				contentType : 'application/json',
				headers : {
					"X-Auth-Token" : $.cookie("JSESSIONID")
				},
				data : form,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data, statusText, xhr) {
					showAlert("Successfully deleted note.", "alert-success",
							"alertContainer", "alertRow");

					const delay = 3000;
					setTimeout(function() {
						window.location.href = "viewNotes.html";

					}, delay);

				},
				error : function(data, statusText, xhr) {

					if (data.status == 409) {
						showAlert("ERROR: Already note subject available.",
								"alert-danger", "alertContainer", "alertRow");
					} else {
						showAlert("An error occured while adding note.",
								"alert-danger", "alertContainer", "alertRow");
					}

				}
			});
		});

$.ajax({
	type : 'POST',
	url : 'get-all-tags',
	headers : {
		"X-Auth-Token" : $.cookie("JSESSIONID")
	},
	beforeSend : function() {
		$('#imgLoaderDiv').show();
	},
	complete : function() {
		$('#imgLoaderDiv').hide();
	},
	success : function(data) {
		$("#LoadingDiv").hide();
		tagsArray = data;
		console.log(data)

		fillTagsCombo();
	},
	error : function(jqXHR, textStatus, errorThrown) {
		showAlert("Error:Server error occur", "alert-danger", "alertContainer",
				"alertRow");

	}
});

function fillTagsCombo() {
	var tags = "";
	tags = "<option value=''>Select Tag</option>";
	for (var i = 0; i < tagsArray.length; i++) {
		tags += "<option value=" + tagsArray[i].id + ">" + tagsArray[i].tagName
				+ "</option>";
	}
	$('select[name="tagId"]').append(tags);
}

$(function() {
    $('#toggle-event').change(function() {
      $('#console-event').html('Toggle: ' + $(this).prop('checked'))
    })
  })
