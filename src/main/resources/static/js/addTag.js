$(document).ready(function() {

});

$("#addTagForm").submit(function(e) {
	e.preventDefault();
	submitForm();
});

function submitForm() {

	var tagName = $("#tagName").val();
	var tagId = $("#tagId").val();

	const tag = {
		"tagName" : tagName,
		"id" : tagId
	};

	console.log(tag);
	saveTag(tag);

}

function saveTag(tag) {

	// calling api to save MI information
	$.ajax({
		type : 'POST',
		url : 'add-tag',
		contentType : 'application/json',
		headers : {
			"X-Auth-Token" : $.cookie("JSESSIONID")
		},
		data : JSON.stringify(tag),
		success : function(data, statusText, xhr) {
			showAlert("Successfully added tag.", "alert-success",
					"alertContainer", "alertRow");

			const delay = 3000;
			setTimeout(function() {
				window.location.href = "addTag.html";

			}, delay);

		},
		error : function(data, statusText, xhr) {

			if (data.status == 409) {
				showAlert("ERROR: Already tag available.", "alert-danger",
						"alertContainer", "alertRow");
			} else {
				showAlert("An error occured while adding tag.", "alert-danger",
						"alertContainer", "alertRow");
			}

		}
	});
}

loadTagstable();

function loadTagstable() {
	$
			.ajax({
				type : 'POST',
				url : 'get-all-tags',
				headers : {
					"X-Auth-Token" : $.cookie("JSESSIONID")
				},
				beforeSend : function() {
					$('#imgLoaderDiv').show();
				},
				complete : function() {
					$('#imgLoaderDiv').hide();
				},
				success : function(data) {
					console.log(data)
					$("#LoadingDiv").hide();
					$('#tagsTable')
							.DataTable(
									{
										"scrollX" : true,
										"scrollY" : "300px",
										data : data,
										order : [ [ 1, "asc" ] ],
										columns : [
												{
													"data" : "ids",
													render : function(data,
															type, row, meta) {
														return meta.row
																+ meta.settings._iDisplayStart
																+ 1;
													}
												},
												{
													data : "tagName"
												},
												{
													data : "noteCount"
												},
												{
													data : "userId",
													"render" : function(data,
															type, full, meta) {
														return data.firstName
																+ "  "
																+ data.lastName;
													}
												},
												{
													data : "updationTime",
													render : function(d) {
														return moment(d)
																.format(
																		"YYYY:MM:DD HH:mm");
													}
												},
												{
													"targets" : -1,
													"data" : null,
													"orderable" : false,
													"searchable" : false,
													"render" : function(data) {
														let action = "";
														action = "<a title='Edit' class='updateBtn' style='cursor: pointer;'><i class='fa fa-edit'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;";

														return action;
													}
												}

										]
									});
				},
				error : function(jqXHR, textStatus, errorThrown) {
					showAlert("Error:Server error occur", "alert-danger",
							"alertContainer", "alertRow");
				}

			// Edit record

			});
}

$('#tagsTable').find('tbody').on(
		'click',
		'.deleteBtn',
		function() {
			if (!confirm("Are you sure?")) {
				return false;
			}
			const data = $('#tagsTable').DataTable().row($(this).parents('tr'))
					.data();

			var id = data.id;

			const tags = {
				"id" : id,
			};

			console.log(id)

			$.ajax({
				type : 'POST',
				url : 'delete-tag',
				contentType : 'application/json',
				headers : {
					"X-Auth-Token" : $.cookie("JSESSIONID")
				},
				data : JSON.stringify(tags),
				success : function(data, statusText, xhr) {
					showAlert("Successfully deleted tag.", "alert-success",
							"alertContainer", "alertRow");

					const delay = 3000;
					setTimeout(function() {
						location.reload();
					}, delay);
				},
				error : function(data, statusText, xhr) {
					showAlert("An error occured while deleting note.",
							"alert-danger", "alertContainer", "alertRow");
				}
			});

		});

$('#tagsTable').find('tbody').on('click', '.updateBtn', function() {
	showOptions();

	const data = $('#tagsTable').DataTable().row($(this).parents('tr')).data();

	$("#tagName").val(data.tagName);
	$("#tagId").val(data.id);

});
$('#showAddDiv').click(function() {
	showOptions();
});

$('#hideAddDiv').click(function() {
	hideOptions();
});
function showOptions() {
	// hideAlert();
	$('#addTag').show();
	$('#hideAddDiv').show();
	$('#showAddDiv').hide();
}

function hideOptions() {
	//hideAlert();
	$('#addTag').hide();
	$('#hideAddDiv').hide();
	$('#showAddDiv').show();
}