$(document).ready(function() {

});

$("#addNoteForm").submit(function(e) {
	e.preventDefault();
	submitForm();
});

function submitForm() {

	var noteSubject = $("#noteSubject").val();
	var note = $("#note").val();
	var tagId = $("#tagId").val();

	
	let form = new FormData();

	form.append("noteSubject", noteSubject);
	form.append("note", note);
	form.append("tagId", tagId);
	form.append("noteId", 0);

	saveNote(form);

}

function saveNote(form) {

	// calling api to save MI information
	$.ajax({
		type : 'POST',
		url : 'add-note',
		contentType : 'application/json',
		headers : {
			"X-Auth-Token" : $.cookie("JSESSIONID")
		},
		data : form,
		cache : false,
		contentType : false,
		processData : false,
		success : function(data, statusText, xhr) {
			showAlert("Successfully added note.", "alert-success",
					"alertContainer", "alertRow");

			const delay = 3000;
			setTimeout(function() {
				window.location.href = "viewNotes.html";

			}, delay);

		},
		error : function(data, statusText, xhr) {
			console.log('in error')
			if (data.status == 409) {
				showAlert("ERROR: Already note subject available.",
						"alert-danger", "alertContainer", "alertRow");
			} else {
				showAlert("An error occured while adding note.",
						"alert-danger", "alertContainer", "alertRow");
			}

		}
	});
}

$.ajax({
	type : 'POST',
	url : 'get-all-tags',
	headers : {
		"X-Auth-Token" : $.cookie("JSESSIONID")
	},
	beforeSend : function() {
		$('#imgLoaderDiv').show();
	},
	complete : function() {
		$('#imgLoaderDiv').hide();
	},
	success : function(data) {
		$("#LoadingDiv").hide();
		tagsArray = data;
		console.log(data)
		
		fillTagsCombo();
	},
	error : function(jqXHR, textStatus, errorThrown) {
		showAlert("Error:Server error occur", "alert-danger", "alertContainer",
				"alertRow");

	}
});

function fillTagsCombo() {
	var tags = "";
	tags = "<option value=''>Select Tag</option>";
	for (var i = 0; i < tagsArray.length; i++) {
		tags += "<option value=" + tagsArray[i].id + ">"
				+ tagsArray[i].tagName + "</option>";
	}
	$('select[name="tagId"]').append(tags);
}
