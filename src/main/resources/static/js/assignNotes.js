//--for data table--//
$(document).ready(function() {

});

$.ajax({
	type : 'POST',
	url : 'get-all-notes-for-assign',
	headers : {
		"X-Auth-Token" : $.cookie("JSESSIONID")
	},
	beforeSend : function() {
		$('#imgLoaderDiv').show();
	},
	complete : function() {
		$('#imgLoaderDiv').hide();
	},
	success : function(data) {
		$("#LoadingDiv").hide();
		notesArray = data;

		fillNotesCombo();
	},
	error : function(jqXHR, textStatus, errorThrown) {
		showAlert("Error:Server error occur", "alert-danger", "alertContainer",
				"alertRow");

	}
});

$.ajax({
	type : 'POST',
	url : 'get-all-users',
	headers : {
		"X-Auth-Token" : $.cookie("JSESSIONID")
	},
	beforeSend : function() {
		$('#imgLoaderDiv').show();
	},
	complete : function() {
		$('#imgLoaderDiv').hide();
	},
	success : function(data) {
		$("#LoadingDiv").hide();
		usersArray = data;

		fillUsersCombo();
	},
	error : function(jqXHR, textStatus, errorThrown) {
		showAlert("Error:Server error occur", "alert-danger", "alertContainer",
				"alertRow");
	}
});

function fillNotesCombo() {
	var notes = "";
	notes = "<option value=''>Select Notes Subject</option>";
	for (var i = 0; i < notesArray.length; i++) {
		notes += "<option value=" + notesArray[i].noteId.id + ">"
				+ notesArray[i].noteId.noteSubject + "</option>";
	}
	$('select[name="noteSubject"]').append(notes);
}

function fillUsersCombo() {
	var users = "";
	users = "<option value=''>Select User</option>";
	for (var i = 0; i < usersArray.length; i++) {
		users += "<option value=" + usersArray[i].userId + ">"
				+ usersArray[i].firstName + "  " + usersArray[i].lastName
				+ "</option>";
	}
	$('select[name="user"]').append(users);
}

$('#btnAssign').click(function() {
	var noteSubject = $("#noteSubject").val();
	var user = $("#user").val();
	var accessLevel = $("#accessLevel").val();

	let form = new FormData();

	form.append("noteId", noteSubject);
	form.append("assignedUserId", user);
	form.append("accessTypeId", accessLevel);

	assignNote(form);

});

function assignNote(form) {

	$.ajax({
		type : 'POST',
		url : 'assign-note',
		contentType : 'application/json',
		headers : {
			"X-Auth-Token" : $.cookie("JSESSIONID")
		},
		data : form,
		cache : false,
		contentType : false,
		processData : false,
		success : function(data, statusText, xhr) {
			showAlert("Successfully assigned note.", "alert-success",
					"alertContainer", "alertRow");

			const delay = 3000;
			setTimeout(function() {
				// window.location.href = "assignedNotesList.html";

			}, delay);

		},
		error : function(data, statusText, xhr) {
			if (data.status == 409) {
				showAlert("Note is already assigned to the selected user.",
						"alert-danger", "alertContainer", "alertRow");

			} else {
				showAlert("An error occured while assigning note.",
						"alert-danger", "alertContainer", "alertRow");

			}
		}
	});
}
